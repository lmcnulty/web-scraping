Example JSON spec for alternativeto.net

```json
{
	"alternatives" : ["//ul[@id='alternativeList']/li", {
		"name" : "//a[@data-link-action='Alternatives']",
		"text" : "//p[@class='text']"
	}]
}
```
