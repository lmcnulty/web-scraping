#echo $(printf '{"items" : [' ; cat doordash-and-pizza-arbitrage.json; printf ","; cat the-cut-psychology-british-empire.json; echo ']}')  | jinja-from-json opengraph.html > article.html


output-json() {
	printf '{"items": ['
	first=""
	while read url; do
		if [ -z "$first" ]; then first="false"; else printf ","; fi
		curl -s "$url" | crapes --schema-json opengraph.json
	done<urls.txt
	printf ']}'
}

output-json | jinja-from-json opengraph.html > articles.html
